# cookiecutter-scraper
Cookiecutter Template for scraper to be used for Local Insights bots.

The repo provides a unified command-line interface for creating your own bot for the Local Insights.

## Create your own scraper.

To create a bear for your favourite language, simply do the following-

1) Install cookiecutter from PyPI-

Get cookiecutter from PyPI through the command-

``pip install cookiecutter``

2) Clone the ``cookiecutter-scraper`` repository-

To create your own coala bears, you will need to clone this repository from Github using-

`` git clone https://github.com/souravsingh/cookiecutter-bear``

3) Run cookiecutter on the cloned repository-

After the repository is cloned, switch to the repository containing the repository using ``cd`` and then run-

`` $ cookiecutter cookiecutter-scraper/ ``